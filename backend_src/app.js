var express = require('express'),
	bodyParser = require('body-parser')
	morgan = require('morgan')
	cors = require('cors');

var session = require('express-session');




var categoryCtrl = require('./apiControllers/categoryController'),
	productCtrl = require('./apiControllers/productController'),
	userCtrl = require('./apiControllers/userController'),
	indexCtrl = require('./apiControllers/indexController');
var	hisCtr=require('./apiControllers/historyController');
var	desCtr=require('./apiControllers/descriptionEditController');
var	favCtr=require('./apiControllers/favProductController');
var	offerCtr=require('./apiControllers/offerProductController');
var	autoAuctionsCtr=require('./apiControllers/autoAuctionsController');
var	beingAuctionedCtr=require('./apiControllers/beingAuctionedController');
var	winCtr=require('./apiControllers/productWinController');
var	ratingCtr=require('./apiControllers/RatingController');
var	blockCtr=require('./apiControllers/blockController');
var	sellingCtr=require('./apiControllers/productSellingController');

var	soldCtr=require('./apiControllers/productSoldController');

var	orderSellCtr=require('./apiControllers/orderSellController');


var app = express();


var server=require("http").Server(app);
//socket
var io = require('socket.io')(server);

io.on('connection', function(client){
  console.log("có người kết nối!")

  	client.on("Client-send-data",data=>{
  			console.log(client.id+ " vừa nói " +data);
  		io.sockets.emit("Server-send-data",data);
  	});


});



app.use(session({
    secret: '2C44-4D44-WppQ38S',
    resave: true,
    saveUninitialized: true
}));

app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.json());

app.get('/', (req, res) => {
	// res.end('hello from nodejs');
	var ret = {
		msg: 'hello from nodejs api'
	};
	res.json(ret);
});
app.use('/index', indexCtrl);

app.use('/categories', categoryCtrl);
app.use('/users', userCtrl);
app.use('/products', productCtrl);
app.use('/histories', hisCtr);
app.use('/descriptions', desCtr);
app.use('/fav', favCtr);
app.use('/offer', offerCtr);
app.use('/autoauctions', autoAuctionsCtr);
app.use('/being', beingAuctionedCtr);
app.use('/win', winCtr);
app.use('/rating', ratingCtr);
app.use('/block', blockCtr);
app.use('/selling', sellingCtr);

app.use('/sold', soldCtr);

app.use('/order', orderSellCtr);


server.listen(3000, () => {
	console.log('API running on port 3000');
});