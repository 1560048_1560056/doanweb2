var express = require('express');
var router = express.Router();
var soldRepo = require('../repos/productSoldRepo');

router.get('/:id', (req, res) => {
    if (req.params.id) {
        var id = req.params.id;

        if (isNaN(id)) {
            res.statusCode = 400;
            res.end();
            return;
        }

    
        soldRepo.load(id).then(rows => {
        
        

            if (rows.length > 0) {
                   var data = {
                        products: rows
           
                    }
                    res.json(data);
                } else {
                    res.statusCode = 204;
                    res.end();
               }
        }).catch(err => {
            console.log(err);
            res.statusCode = 500;
            res.end('View error log on console.');
        });
    }else {
        res.statusCode = 400;
        res.json('error');
    }
});






router.post('/search', (req, res) => {
    soldRepo.Search(req.body)
        .then(rows => {
            res.statusCode = 200;
           var data = {
                products: rows
           
            }
            res.json(data);
        })
        .catch(err => {
            console.log(err);
            res.statusCode = 500;
            res.end();
        });
});

module.exports = router;