var express = require('express'),
axios = require('axios');



var userRepo = require('../repos/userRepo');

var router = express.Router();

router.post('/username', (req, res) => {
     
        userRepo.getUserName1(req.body.username).then(rows => {
            res.json({
                users: rows
            });
        }).catch(err => {
            console.log(err);
            res.statusCode = 500;
            res.end();
        });
   
});






router.post('/register', (req, res) => {
    userRepo.add(req.body)
    .then(insertId => {
        res.statusCode = 201;
        res.json(req.body);
    })
    .catch(err => {
        console.log(err);
        res.statusCode = 500;
        res.end();
    });
});

router.post('/login', (req, res) => {
    userRepo.get(req.body)

        .then(rows => {
            if(rows.length > 0){
                req.session.id=rows[0].f_ID;
                var data = {
                    f_ID: rows[0].f_ID,
                    f_IDSession: req.session.id,
                    f_Username: rows[0].f_Username,
                    f_Permission:rows[0].f_Permission,
                    f_Name : rows[0].f_Name,
                    f_Email : rows[0].f_Email,
                    f_Password:rows[0].f_Password
                }
                
                console.log( req.session.id);
                 console.log( rows[0].f_Username);
                res.statusCode = 201;
                res.json(data);  
            }  

    
    })
    .catch(err => {
        console.log(err);
        res.statusCode = 500;
        res.end();
    });
});

router.post('/logout', (req, res) => {
    req.session.destroy();
    res.statusCode = 201;
    res.json(true);
});

router.post('/checkUserName', (req, res) => {
    userRepo.getUserName(req.body)
    .then(rows => {
        res.statusCode = 201;
        if(rows.length > 0){
            res.json(true);    
        } else {
            res.json(false);
        }
    })
    .catch(err => {
        console.log(err);
        res.statusCode = 500;
        res.end();
    });
})

router.post('/checkAddress', (req, res) => {
    userRepo.getAddress(req.body)
    .then(rows => {
        res.statusCode = 201;
        if(rows.length > 0){
            res.json(true);    
        } else {
            res.json(false);
        }
    })
    .catch(err => {
        console.log(err);
        res.statusCode = 500;
        res.end();
    });
})

router.post('/captcha', (req, res) => {
    var secret = '6LeK3l8UAAAAADhMkaYbJNkmXXA1TfGrdvb0xWO9';
    var captcha_response = req.body.captcha_response;

    var url = `https://www.google.com/recaptcha/api/siteverify?secret=${secret}&response=${captcha_response}`;
    axios.post(url, {
            // secret: _secret,
            // response: captcha_response
        }, {
        	headers: {
        		"Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
        	}
        })
    .then(function(response) {
            // console.log(response.data);
            // res.end('ok');
            res.json(response.data);
        })
    .catch(function(error) {
        res.end('fail');
    });
});

router.get('/getAll', (req, res) => {
    userRepo.getAll().then(rows => {
        res.json(rows);
        res.statusCode = 204;
        res.end();
    }).catch(err => {
        console.log(err);
        res.statusCode = 500;
        res.end();
    });
});

router.delete('/delete/:id',(req,res) => {
    if(req.params.id){
        var id = req.params.id;
        if(isNaN(id)){
            res.statusCode = 400;
            res.end('View error log on console');
            return;
        }
        userRepo.delete(id).then(affectedRows => {
            res.json({
                affectedRows: affectedRows
            });
            res.statusCode = 204;
            res.end();
        }).catch(err => {
            console.log(err);
            res.statusCode = 500;
            res.json('error');
        });
    } else {
        console.log(err);
        res.statusCode = 500;
        res.end();
    }
});

router.put('/reset_password/:id',(req,res) => {
    if(req.params.id){
        var id = req.params.id;
        if(isNaN(id)){
            res.statusCode = 400;
            res.end('View error log on console');
            return;
        }
        userRepo.reset(req.body,id).then(affectedRows => {
            res.json({
                affectedRows: affectedRows
            });
        }).catch(err => {
            console.log(err);
            res.statusCode = 500;
            res.end();
        });
    }  else {
        console.log(err);
        res.statusCode = 500;
        res.end();
    }
});

router.put('/updatePermission/:id',(req,res) => {
    if(req.params.id){
        var id = req.params.id;
        if(isNaN(id)){
            res.statusCode = 400;
            res.end('View error log on console');
            return;
        }
        userRepo.updatePermission(req.body,id).then(affectedRows => {
            res.json({
                affectedRows: affectedRows
            });
        }).catch(err => {
            console.log(err);
            res.statusCode = 500;
            res.end();
        });
    }  else {
        console.log(err);
        res.statusCode = 500;
        res.end();
    }
});

router.put('/:id',(req,res) => {
    if(req.params.id){
        var id = req.params.id;
        if(isNaN(id)){
            res.statusCode = 400;
            res.end('View error log on console');
            return;
        }
        userRepo.update(req.body,id).then(affectedRows => {
            res.json({
                affectedRows: affectedRows
            });
        }).catch(err => {
            console.log(err);
            res.statusCode = 500;
            res.end();
        });
    }  else {
        console.log(err);
        res.statusCode = 500;
        res.end();
    }
});

router.post('/checkPassword', (req, res) => {
    userRepo.get(req.body)
    .then(rows => {
        if(rows.length > 0){
            res.statusCode = 201;
            res.json(data);    
        }
    })
    .catch(err => {
        console.log(err);
        res.statusCode = 500;
        res.end();
    });
});

module.exports = router;