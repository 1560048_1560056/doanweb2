var express = require('express');
var router = express.Router();
var favRepo = require('../repos/favRepo');

router.get('/:id', (req, res) => {
    if (req.params.id) {
        var id = req.params.id;

        if (isNaN(id)) {
            res.statusCode = 400;
            res.end();
            return;
        }

    
        favRepo.load(id).then(rows => {
        
        

            if (rows.length > 0) {
                   var data = {
                        products: rows
           
                    }
                    res.json(data);
                } else {
                    res.statusCode = 204;
                    res.end();
               }
        }).catch(err => {
            console.log(err);
            res.statusCode = 500;
            res.end('View error log on console.');
        });
    }else {
        res.statusCode = 400;
        res.json('error');
    }
});

router.get('/', (req, res) => {
   if (req.query.idu&&req.query.idpro) {

        var idu = req.query.idu;
        var idpro=req.query.idpro;
        

        favRepo.loadid(idu,idpro).then(rows => {
           if (rows!=null) {
                   
                    res.json(rows);
                } else {
                    res.statusCode = 204;
                    res.end();
               }
        }).catch(err => {
            console.log(err);
            res.statusCode = 500;
            res.json('error');
        });
    } else {
        res.statusCode = 400;
        res.json('error');
        console.log(req.query);
    }
});


router.post('/addfav', (req, res) => {
    favRepo.insert(req.body)
        .then(insertId => {
            res.statusCode = 201;
            res.json(req.body);
        })
        .catch(err => {
            console.log(err);
            res.statusCode = 500;
            res.end();
        });
});

router.delete('/removefav', (req, res) => {
    console.log("có xóa!");
    favRepo.Delete(req.body)
        .then(insertId => {
            res.statusCode = 201;
            res.json(req.body);
        })
        .catch(err => {
            console.log(err);
            res.statusCode = 500;
            res.end();
        });
});

router.post('/search', (req, res) => {
    favRepo.Search(req.body)
        .then(rows => {
            res.statusCode = 200;
           var data = {
                products: rows
           
            }
            res.json(data);
        })
        .catch(err => {
            console.log(err);
            res.statusCode = 500;
            res.end();
        });
});

module.exports = router;