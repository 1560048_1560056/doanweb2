var express = require('express');
var categoryRepo = require('../repos/categoryRepo'),
constants = require('../fn/const');

var router = express.Router();

router.get('/', (req, res) => {

	if (req.query.idcat&&req.query.page) {
		var idcat = req.query.idcat;
		var page=req.query.page;


		categoryRepo.loadCategoryProduct(idcat,page).then(rows => {
			if (rows.length > 0) {
				var hasMore = rows.length > constants.PRODUCTS_PER_PAGE;
				if (hasMore) {
					rows.pop();
				}


				res.json({products: rows, hasMore: hasMore});
			} else {
				res.statusCode = 204;
				res.end();
			}
		}).catch(err => {

			res.statusCode = 500;
			res.json('error');
		});
	}else{

		categoryRepo.loadAll().then(rows => {
			res.json(rows);
		}).catch(err => {
			console.log(err);
			res.statusCode = 500;
			res.end('View error log on console.');
		});
	}
	
});

router.get('/getAll', (req, res) => {
	categoryRepo.loadAll().then(rows => {
		res.json(rows);
		res.statusCode = 204;
		res.end();
	}).catch(err => {
		console.log(err);
		res.statusCode = 500;
		res.end();
	});
});


// 
// categories/5

router.get('/:id', (req, res) => {
	if (req.params.id) {
		var id = req.params.id;

		if (isNaN(id)) {
			res.statusCode = 400;
			res.end();
			return;
		}

		categoryRepo.load(id).then(rows => {
			if (rows.length > 0) {
				res.json(rows[0]);
			} else {
				res.statusCode = 204;
				res.end();
			}
		}).catch(err => {
			console.log(err);
			res.statusCode = 500;
			res.json('error');
		});
	} else {
		res.statusCode = 400;
		res.json('error');
	}
});

router.post('/', (req, res) => {
	categoryRepo.add(req.body)
	.then(insertId => {
		var poco = {
			CatID: insertId,
			CatName: req.body.CatName
		};
		res.statusCode = 201;
		res.json(poco);
	})
	.catch(err => {
		console.log(err);
		res.statusCode = 500;
		res.end();
	});
});


router.delete('/:id', (req, res) => {
	if (req.params.id) {
		var id = req.params.id;

		if (isNaN(id)) {
			res.statusCode = 400;
			res.end();
			return;
		}

		categoryRepo.delete(id).then(affectedRows => {
			res.json({
				affectedRows: affectedRows
			});
		}).catch(err => {
			console.log(err);
			res.statusCode = 500;
			res.json('error');
		});
	} else {
		res.statusCode = 400;
		res.json('error');
	}
});

router.get('/getAll', (req, res) => {
    categoryRepo.getAll().then(rows => {
        res.json(rows);
        res.statusCode = 204;
        res.end();
    }).catch(err => {
        console.log(err);
        res.statusCode = 500;
        res.end();
    });
});

router.put('/update/:id',(req,res) => {
    if(req.params.id){
        var id = req.params.id;
        if(isNaN(id)){
            res.statusCode = 400;
            res.end('View error log on console');
            return;
        }
        categoryRepo.update(req.body,id).then(affectedRows => {
            res.json({
                affectedRows: affectedRows
            });
        }).catch(err => {
            console.log(err);
            res.statusCode = 500;
            res.end();
        });
    }  else {
        console.log(err);
        res.statusCode = 500;
        res.end();
    }
});

module.exports = router;



/*categoryRepo.loadAll().then(rows => {
		res.json(rows);
		res.statusCode = 204;
		res.end();
	}).catch(err => {
		console.log(err);
		res.statusCode = 500;
		res.end();
	});*/