
var express = require('express');
var router = express.Router();
var productRepo = require('../repos/productRepo');

router.get('/loadTop5Quantyti', (req, res) => {
    
    productRepo.loadTop5Quantyti().then(rows => {
        
        

        if (rows.length > 0) {
                var data = {
                products: rows
           
                }
                res.json(data);
            } else {
                res.statusCode = 204;
                res.end();
            }
    }).catch(err => {
        console.log(err);
        res.statusCode = 500;
        res.end('View error log on console.');
    });
});

router.get('/loadTop5Price', (req, res) => {
    
    productRepo.loadTop5Price().then(rows => {
        
        
        
        if (rows.length > 0) {
                var data = {
                products: rows
           
                }
                res.json(data);
            } else {
                res.statusCode = 204;
                res.end();
            }
    }).catch(err => {
        console.log(err);
        res.statusCode = 500;
        res.end('View error log on console.');
    });
});

router.get('/loadTop5Time', (req, res) => {
    
    productRepo.loadTop5Time().then(rows => {
        
        
        
        if (rows.length > 0) {
                 var data = {
                products: rows
           
                }
                res.json(data);
            } else {
                res.statusCode = 204;
                res.end();
            }
    }).catch(err => {
        console.log(err);
        res.statusCode = 500;
        res.end('View error log on console.');
    });
});

module.exports = router;