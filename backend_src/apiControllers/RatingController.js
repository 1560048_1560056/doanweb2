var express = require('express');
var router = express.Router();
var ratingRepo = require('../repos/ratingRepo');



router.post('/', (req, res) => {
    ratingRepo.add(req.body)
        .then(insertId => {
            res.statusCode = 201;
           

            res.json(insertId);
        })
        .catch(err => {
            console.log(err);
            res.statusCode = 500;
            res.end();
        });
});

module.exports = router;