var md5 = require('md5');
var db = require('../fn/mysql-db');

exports.add = function (poco) {
	var md5_password = md5(poco.Password);

	var sql = `insert into users(f_Username, f_Password, f_Name, f_Email, f_Permission,f_Mark,f_Address) values('${poco.Username}', '${md5_password}', '${poco.Name}', '${poco.Email}', '${poco.Permission}','${poco.Mark}','${poco.Address}')`;
	return db.insert(sql);
}

exports.get = function (poco){
	var md5_password = md5(poco.Password);

	var sql = `select * from users where f_Username='${poco.Username}' and f_Password='${md5_password}'`;
	return db.load(sql);
}

exports.getUserName = function (poco){

	var sql = `select * from users where f_Username='${poco.Username}'`;
	return db.load(sql);
}

exports.getUserName1 = function (username){

	var sql = `select *,a.f_Username names, b.f_Username namek,a.f_Email emaila, a.f_Address addressa,a.f_Mark marka,b.f_Email emailb, b.f_Address addressb,b.f_Mark markb  from users a Left join comments on comments.f_ID=a.f_ID Left join users b on  b.f_ID=comments.keeper
 where a.f_Username='${username}'`;
	return db.load(sql);
}

exports.getAddress = function (poco){

	var sql = `select * from users where f_Address='${poco.Address}'`;
	return db.load(sql);
}

exports.getKeeper = function (poco){

	var sql = `select * from users,products where f_ID=keeper and ProID=${poco.ProID}`;
	return db.load(sql);
}

exports.getKick = function (poco){

	var sql = `select * from users,products where f_ID=${poco.f_ID} and ProID=${poco.ProID}`;
	return db.load(sql);
}

exports.getSeller = function (poco){

	var sql = `select *,us1.f_Email sellemail, us2.f_Email keepemail,us1.f_Username sellname, us2.f_Username keepname   from users us1,products,users us2 where us1.f_ID=Seller and ProID=${poco.ProID} and us2.f_ID=keeper`;

	return db.load(sql);
}

exports.getuser = function (id){

	var sql = `select * from users where f_ID=${id}`;
	return db.load(sql);
}

exports.getAll = function(){
	var sql = `select * from users`;
	return db.load(sql);
}

exports.delete = function(id){
	var sql = `delete from users where f_ID = ${id}`;
	return db.delete(sql);
}

exports.reset = function(poco,id){
	var md5_password = md5(poco.Password);
	var sql = `update users set f_Password = '${md5_password}' where f_ID = ${id}`;
	return db.update(sql);
}

exports.updatePermission = function(poco,id){
	var sql = `update users set f_Permission = 1,f_BeginTimeOrderSell = '${poco.BeginDate}',f_EndTimeOrderSell = '${poco.EndDate}' where f_ID = ${id}`;
	return db.update(sql);
}

exports.update = function(poco,id){
	var sql = `update users set f_Name = '${poco.Name}',f_Email = '${poco.Email}' where f_ID = ${id}`;
	return db.update(sql);
}
