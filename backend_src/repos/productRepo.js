var db = require('../fn/mysql-db'),
    constants = require('../fn/const');

exports.loadAll = function() {
    var sql = 'select * from products';
    return db.load(sql);
}

exports.loadAllName = function() {
    var sql = 'select ProName from products';
    return db.load(sql);
}


exports.loadPage = function(page) {
    var offset = (page - 1) * constants.PRODUCTS_PER_PAGE;
    var sql = `select * ,TIMESTAMPDIFF(HOUR,CURRENT_TIMESTAMP, EndTime) existstime,TIMESTAMPDIFF(MINUTE,CURRENT_TIMESTAMP,EndTime)%60 existsminute from products LEFT JOIN users on f_ID=keeper ORDER BY PostTime desc  limit ${constants.PRODUCTS_PER_PAGE + 1} offset ${offset}`;
    return db.load(sql);
}

exports.load = function(id) {

    var sql = `select *,TIMESTAMPDIFF(HOUR,CURRENT_TIMESTAMP, EndTime) existstime,TIMESTAMPDIFF(MINUTE,CURRENT_TIMESTAMP,EndTime)%60 existsminute ,sler.f_Username selname,k.f_Username keepname,sler.f_Email sellemail   from (products LEFT JOIN users k on f_ID=keeper) ,users  sler where ProID = ${id} and sler.f_ID=Seller`;
    
    return db.load(sql);
}


exports.loadSearch = function(name,page) {
	var offset = (page - 1) * constants.PRODUCTS_PER_PAGE;
    var sql = `select * ,TIMESTAMPDIFF(HOUR,CURRENT_TIMESTAMP, EndTime) existstime,TIMESTAMPDIFF(MINUTE,CURRENT_TIMESTAMP,EndTime)%60 existsminute from products LEFT JOIN users on f_ID=keeper where ProName LIKE '%${name}%' limit ${constants.PRODUCTS_PER_PAGE + 1} offset ${offset}`;
    return db.load(sql);
}



exports.loadTop5Quantyti = function() {
   
    var sql = `select *,TIMESTAMPDIFF(HOUR,CURRENT_TIMESTAMP, EndTime) existstime,TIMESTAMPDIFF(MINUTE,CURRENT_TIMESTAMP,EndTime)%60 existsminute from products LEFT JOIN users on f_ID=keeper where EndTime >CURRENT_TIMESTAMP   ORDER BY  Quantity desc limit 5 offset 0` ;
    return db.load(sql);
}



exports.loadTop5Price = function() {
   
    var sql = `select *,TIMESTAMPDIFF(HOUR,CURRENT_TIMESTAMP, EndTime) existstime,TIMESTAMPDIFF(MINUTE,CURRENT_TIMESTAMP,EndTime)%60 existsminute from products LEFT JOIN users on f_ID=keeper where EndTime >CURRENT_TIMESTAMP   ORDER BY  Price desc limit 5 offset 0 `; 

    return db.load(sql);
}

exports.loadTop5Time = function() {
   
    var sql = `select *,TIMESTAMPDIFF(HOUR,CURRENT_TIMESTAMP, EndTime) existstime,TIMESTAMPDIFF(MINUTE,CURRENT_TIMESTAMP,EndTime)%60 existsminute from products LEFT JOIN users on f_ID=keeper where EndTime >CURRENT_TIMESTAMP   ORDER BY  EndTime asc limit 5 offset 0 ` ;



    return db.load(sql);
}

exports.add = function(poco){
    if(isNaN(poco.BestPrice)){
        poco.BestPrice=null;
    }
    var sql = `insert into products(ProName,TinyDes,Price,Seller,PriceJumb,PriceNow,PostTime,EndTime,CatID,Auto,FirstPrice) values('${poco.NameProduct}','${poco.Des}',${poco.PriceBase},${poco.Seller},${poco.Range},${poco.BestPrice},'${poco.PostTime}','${poco.EndTime}','${poco.CatID}','${poco.Auto}',${poco.PriceBase})`;
    return db.insert(sql);
}

exports.updateDateAuction = function(date,idpro){
    //2018-06-07 15:01:15
    var s=date.getSeconds();
    var mn=date.getMinutes();
    var h=date.getHours();
    var d=date.getDate();
    var M=date.getMonth()+1;
    var Y=date.getFullYear();
    var datest=`${Y}-${M}-${d} ${h}:${mn}:${s}`;
    var sql = `update products set EndTime='${datest}' where ProID=${idpro}`;
    return db.insert(sql);
    //console.log(date.getSeconds());
}


exports.edit = function(poco){
    var sql = `update products set TinyDes='${poco.content}' where ProID=${poco.ProID}`;
    return db.update(sql);
}

exports.getAutoID = function(){
    var sql = `Select MAX(ProID) as maxID From products`;
    return db.load(sql);
}