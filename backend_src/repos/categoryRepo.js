var db = require('../fn/mysql-db'),
	constants = require('../fn/const');

exports.loadAll = function() {
	var sql = 'select * from categories';
	return db.load(sql);
}

exports.load = function(id) {
	var sql = `select * from categories where CatID = ${id}`;
	return db.load(sql);
}

exports.loadCategoryProduct = function(idcat,page) {
    var offset = (page - 1) * constants.PRODUCTS_PER_PAGE;
    var sql = `select * ,TIMESTAMPDIFF(HOUR,CURRENT_TIMESTAMP, EndTime) existstime from (products LEFT JOIN users on f_ID=keeper), categories where  products.CatID=${idcat} and products.CatID=categories.CatID limit ${constants.PRODUCTS_PER_PAGE + 1} offset ${offset}`;
    return db.load(sql);
}

exports.add = function(poco) {
	// poco = {
	// 	CatID: 1,
	// 	CatName: 'new name'
	// }
	
	var sql = `insert into categories(CatName) values('${poco.CatName}')`;
	return db.insert(sql);
}

exports.delete = function(id) {
	var sql = `delete from categories where CatID = ${id}`;
	return db.delete(sql);
}

exports.getAll = function(){
	var sql = `select * from categories`;
	return db.load(sql);
}

exports.update = function(poco,id){
	var sql = `update categories set CatName='${poco.CatName}' where CatID = ${id}`;
	return db.update(sql);
}