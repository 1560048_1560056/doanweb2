var db = require('../fn/mysql-db'),
    constants = require('../fn/const');

exports.load = function(idpro) {
    var sql = `select * from auto_auctions,products where auto_auctions.ProID=${idpro} and auto_auctions.ProID=products.ProID  ORDER BY PriceHighest desc , auto_date asc`;
    return db.load(sql);
}

exports.load2 = function(idpro,idu) {
    var sql = `select * from auto_auctions,products where auto_auctions.ProID=${idpro} and auto_auctions.f_ID=${idu}  and auto_auctions.ProID=products.ProID  ORDER BY PriceHighest desc , auto_date asc`;
    return db.load(sql);
}

exports.add = function (poco) {
	

	var sql = `insert into auto_auctions(ProID,f_ID,PriceHighest,auto_date) values(${poco.ProID}, ${poco.f_ID},${poco.PriceHighest},CURRENT_TIMESTAMP)`;
	return db.insert(sql);
}