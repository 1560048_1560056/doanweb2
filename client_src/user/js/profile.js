$( document ).ready(function() {
	$('#sa-success').hide();
	var t_name = localStorage.getItem('f_Name');
	var t_email = localStorage.getItem('f_Email');
	$('#name').val(t_name);
	$('#email').val(t_email);
})

$('#submit').click(function(){
	checkValidate();
	if($('#updateForm').valid())
	{
		var id = localStorage.getItem('f_ID');
		var name = $('#name').val();
		var email = $('#email').val();
		var body = {
			Name : name,
			Email : email,
		};
		$.ajax({
			url: 'http://localhost:3000/users/' + id,
			dataType: 'json',
			timeout: 10000,
			type: 'PUT',
			contentType: 'application/json',
			data: JSON.stringify(body),
		}).done(function(data) {
			$('#sa-success').trigger('click');
		}).fail(function(xhr, textStatus, error) {
			console.log(textStatus);
			console.log(error);
			console.log(xhr);
		});
		return false;
	}
})

function checkValidate(){
	$('#updateForm').validate({
		rules: {
			name: {
				required: true,
			},
			email: {
				required: true,
				email: true,
			},
		},
		messages: {
			name: {
				required: 'Input name',
			},
			email: {
				required: 'Input email',
				email: 'Email not right',
			},
		},

		highlight: function (element) {
			$(element)
			.closest('.form-group')
			.addClass('has-error');
		},

		success: function (label) {
			label.closest('.form-group').removeClass('has-error');
			label.remove();
		},

	})
}

$('#change_pass').click(function(){
	$('#name').val('');
	$('#email').val('');
	window.location.href = "./change-password.html";
})