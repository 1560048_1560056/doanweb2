$( document ).ready(function() {
	loadCategory();
});


/*$('#submit').click(function(){
	checkValidate();
	if($('#frmAddProduct').valid())
	{
		var name_product = $('#name_product').val();
		var des_product = $('#des_product').val();
		var price_base = $('#price_base').val();
		var range = $('#range').val();
		var best_price = $('#best_price').val();
		var time = new Date();
		time = formatDateToDateTime(time);
		var date_number = $('#date_number').val();
		var end_time = new Date();
		var day = end_time.getDate() + parseInt(date_number);
		end_time.setDate(day);
		end_time = formatDateToDateTime(end_time);
		var seller = localStorage.getItem('f_ID');
		var cat_id = $('#category').val();
		var images = [];
		var files = document.getElementById('image_product').files || [];
		$.each(files,function(i,myfile){
			images.push(myfile.name);
		});
		var body = {
			NameProduct : name_product,
			Des : des_product,
			PriceBase : price_base,
			Range : range,
			BestPrice : best_price,
			PostTime : time,
			EndTime : end_time,
			Seller : seller,
			CatID : cat_id,
			Images : images,
		};
		$.ajax({
			url: 'http://localhost:3000/products/addProduct',
			dataType: 'json',
			timeout: 10000,
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(body),
		}).done(function(data) {
			$('#sa-success').trigger('click');
			$('#myremove').trigger('click');
			$('#frmAddProduct')[0].reset();
		}).fail(function(xhr, textStatus, error) {
			console.log(textStatus);
			console.log(error);
			console.log(xhr);
		});
		return false;
	}
})*/

$('#frmAddProduct').submit(function(e){
	checkValidate();
	if(!checkNumberImages()){
		$('#error_images').show();
	} else {
		$('#error_images').hide();
	}
	if($('#frmAddProduct').valid()){
		e.preventDefault();
		var formData = new FormData(this);
		var seller_id = localStorage.getItem('f_ID');
		formData.append('seller',seller_id);
		var time = new Date();
		time = formatDateToDateTime(time);
		formData.append('post_time',time);
		var date_number = $('#date_number').val();
		var end_date = new Date();
		var day = end_date.getDate() + parseInt(date_number);
		end_date.setDate(day);
		end_date = formatDateToDateTime(end_date);
		formData.append('end_time',end_date);
		var auto = $('#auto:checkbox:checked').length;
		formData.append('auto',auto);
		$.ajax({
			type: "POST",
			url: 'http://localhost:3000/products/addProduct',
			data: formData,
			processData: false,
			contentType: false,
		}).done(function(data) {
			$('#sa-success').trigger('click');
			$('#myremove').trigger('click');
			$('#frmAddProduct')[0].reset();
		}).fail(function(xhr, textStatus, error) {
			console.log(textStatus);
			console.log(error);
			console.log(xhr);
		});
	}
	return false;
});


function checkValidate(){

	$('#frmAddProduct').validate({
		rules: {
			name_product: {
				required: true,
			},
			des_product: {
				required: true,
			},
			price_base: {
				required: true,
			},
			range: {
				required: true,
			},
			
			image_product: {
				required: true,
			},
			date_number: {
				required: true,
			},
		},
		messages: {
			name_product: {
				required: 'Input name product',
			},
			des_product: {
				required: 'Input description',
			},
			price_base: {
				required: 'Input price start',
			},
			range: {
				required: 'Input range increase',
			},
			
			image_product: {
				required: 'Choose image product',
			},
			date_number: {
				required: 'Input number of day',
			},
		},

		highlight: function (element) {
			$(element)
			.closest('.form-group')
			.addClass('has-error');
		},

		success: function (label) {
		},


	});
}

function formatDateToDateTime(date){
	var s=date.getSeconds();
	var mn=date.getMinutes();
	var h=date.getHours();
	var d=date.getDate();
	var M=date.getMonth()+1;
	var Y=date.getFullYear();
	var date_time=`${Y}-${M}-${d} ${h}:${mn}:${s}`;
	return date_time;
}

function loadCategory(){
	$.ajax({
		url: 'http://localhost:3000/categories/getAll',
		dataType: 'json',
		timeout: 10000
	}).done(function(data) {
		for(var i=0;i<data.length;i++){
			$('#category').append($('<option>', {
				value: data[i].CatID,
				text: data[i].CatName,
			}));
		}
	}).fail(function(xhr, textStatus, error) {
		console.log(textStatus);
		console.log(error);
		console.log(xhr);
	});
}

function checkNumberImages(){
	var files = $('#images')[0].files;
	if(files.length < 3){
		return false;
	}
	return true;
}