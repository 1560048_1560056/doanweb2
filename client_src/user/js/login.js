$( document ).ready(function() {
	$('#sa-success').hide();
});

$('#submit').click(function(){
	checkValidate();
	if($('#loginForm').valid())
	{
		var username = $('#username').val();
		var password = $('#password').val();
		var body = {
			Username : username,
			Password : password,
		};
		$.ajax({
			url: 'http://localhost:3000/users/login',
			dataType: 'json',
			timeout: 10000,
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(body),
		}).done(function(data) {
			localStorage.setItem('f_ID',data.f_ID);
			localStorage.setItem('f_IDSession',data.f_IDSession);
			localStorage.setItem('f_Username',data.f_Username);

			localStorage.setItem('f_Permission',data.f_Permission);
			

			localStorage.setItem('f_Name',data.f_Name);
			localStorage.setItem('f_Email',data.f_Email);
			localStorage.setItem('f_Password',data.f_Password);

			$('#sa-success').trigger('click');
			$('#loginForm')[0].reset();
			if(data.f_Permission<2){
				document.location="/";
			}
			else{
				document.location="/admin/order_sell.html";
			}
			
		}).fail(function(xhr, textStatus, error) {
			console.log(textStatus);
			console.log(error);
			console.log(xhr);
		});
		return false;
	}
})

function checkValidate(){
	$('#loginForm').validate({
		rules: {
			username: {
				required: true,
				minlength: 3,
			},
			password: {
				required: true,
				minlength: 6,
			},
		},
		messages: {
			username: {
				required: 'Input username',
				minlength: 'Min length is 3',
			},
			password: {
				required: 'Input password',
				minlength: 'Min length is 6',
			},
		},

		highlight: function (element) {
			$(element)
			.closest('.form-group')
			.addClass('has-error');
		},

		success: function (label) {
		},


	})
}