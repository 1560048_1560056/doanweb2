$(document).ready(function() {
	$('#sa-success').hide();
})

$('#send').click(function(){
	var id = localStorage.getItem('f_ID');
	var username = localStorage.getItem('f_Username');
	var time = new Date();
	time = formatDateToDateTime(time);
	var body = {
		ID : id,
		Username : username,
		Time : time,
	};
	$.ajax({
		url: 'http://localhost:3000/order/',
		dataType: 'json',
		timeout: 10000,
		type: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(body),
	}).done(function(data) {
		$('#sa-success').trigger('click');
	}).fail(function(xhr, textStatus, error) {
		console.log(textStatus);
		console.log(error);
		console.log(xhr);
	});
	return false;
})

function formatDateToDateTime(date){
	var s=date.getSeconds();
	var mn=date.getMinutes();
	var h=date.getHours();
	var d=date.getDate();
	var M=date.getMonth()+1;
	var Y=date.getFullYear();
	var date_time=`${Y}-${M}-${d} ${h}:${mn}:${s}`;
	return date_time;
}