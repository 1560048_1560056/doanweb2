$( document ).ready(function() {
	$('#sa-success').hide();

	$.validator.addMethod(
		"uniqueUserName", 
		function(value, element) {
			var response = true;
			var username = $('#username').val();
			var body = {
				Username: username,
			}
			$.ajax({
				url: 'http://localhost:3000/users/checkUserName',
				dataType: 'json',
				timeout: 10000,
				type: 'POST',
				async: false,
				contentType: 'application/json',
				data: JSON.stringify(body),
				success: function(data){
					if(data){
						response = false;
					}
				}
			});
			return response;
		}
	);

	$.validator.addMethod(
		"uniqueAddress", 
		function(value, element) {
			var response = true;
			var address = $('#address').val();
			var body = {
				Address: address,
			}
			$.ajax({
				url: 'http://localhost:3000/users/checkAddress',
				dataType: 'json',
				timeout: 10000,
				type: 'POST',
				async: false,
				contentType: 'application/json',
				data: JSON.stringify(body),
				success: function(data){
					if(data){
						response = false;
					}
				}
			});
			return response;
		}
		);
});

$('#submit').click(function(){
	checkValidate();
	if($('#registerForm').valid())
	{
		if(checkRecaptcha())
		{
			var username = $('#username').val();
			var password = $('#password').val();
			var name = $('#name').val();
			var email = $('#email').val();
			var address = $('#address').val();
			var body = {
				Username : username,
				Password : password,
				Name : name,
				Email : email,
				Permission: '0',
				Mark: '0',
				Address: address,
			};
			$.ajax({
				url: 'http://localhost:3000/users/register',
				dataType: 'json',
				timeout: 10000,
				type: 'POST',
				contentType: 'application/json',
				data: JSON.stringify(body),
			}).done(function(data) {
				$('#sa-success').trigger('click');
				$('#registerForm')[0].reset();
			}).fail(function(xhr, textStatus, error) {
				console.log(textStatus);
				console.log(error);
				console.log(xhr);
			});
		}
		return false;
	}
})

function checkValidate(){

	$('#registerForm').validate({
		rules: {
			username: {
				required: true,
				minlength: 3,
				uniqueUserName: true,
			},
			password: {
				required: true,
				minlength: 6,
			},
			name: {
				required: true,
			},
			email: {
				required: true,
				email: true,
			},
			address: {
				required: true,
				uniqueAddress: true,
			}
		},
		messages: {
			username: {
				required: 'Input username',
				minlength: 'Min length is 3',
				uniqueUserName: 'This username is taken already',
			},
			password: {
				required: 'Input password',
				minlength: 'Min length is 6',
			},
			name: {
				required: 'Input name',
			},
			email: {
				required: 'Input email',
				email: 'Email not right',
			},
			address: {
				required: 'Input address',
				uniqueAddress: 'This address is taken already',
			}
		},

		highlight: function (element) {
			$(element)
				.closest('.form-group')
				.addClass('has-error');
        },

        success: function (label) {
        	label.closest('.form-group').removeClass('has-error');
            label.remove();
        },


	})
}


function checkRecaptcha(){
	var response = false;
	var body = {
		captcha_response: grecaptcha.getResponse()
	};

	$.ajax({
		url: 'http://localhost:3000/users/captcha',
		dataType: 'json',
		timeout: 10000,
		type: 'POST',
		async: false,
		contentType: 'application/json',
		data: JSON.stringify(body)
	}).done(function(data) {
		if (data.success) {
			grecaptcha.reset();
			response = true;
		} else {
			grecaptcha.reset();
			swal("Invalid captcha.", "You clicked the button!", "error");
		}
	}).fail(function(xhr, textStatus, error) {
		console.log(textStatus);
		console.log(error);
		console.log(xhr);
	});
    return response;
}