$( document ).ready(function() {
	$('#sa-success').hide();
	$('#sa-title').hide();
});

$('#submit').click(function(){
	checkValidate();
	if($('#changePassForm').valid()){
		var username = localStorage.getItem('f_Username');
		var password = $('#new_password').val();
		var id = localStorage.getItem('f_ID');

		var body = {
			Password : password,
		};
		$.ajax({
			url: 'http://localhost:3000/users/reset_password/' + id,
			dataType: 'json',
			timeout: 10000,
			type: 'PUT',
			contentType: 'application/json',
			data: JSON.stringify(body),
		}).done(function(data) {
			$('#sa-success').trigger('click');
			$('#changePassForm')[0].reset();
		}).fail(function(xhr, textStatus, error) {
			console.log(textStatus);
			console.log(error);
			console.log(xhr);
		});
		
		return false;
	}
})

function checkValidate(){
	$('#changePassForm').validate({
		rules: {
			password: {
				required: true,
				minlength: 6,
			},
			new_password: {
				required: true,
				minlength: 6,
			},
			confirm_password: {
				required: true,
				equalTo: $('#new_password'),
			},
		},
		messages: {
			password: {
				required: 'Input password',
				minlength: 'Min length is 6',
			},
			new_password: {
				required: 'Input password',
				minlength: 'Min length is 6',
			},
			confirm_password: {
				required: 'Input password',
				equalTo: 'Confirm password is wrong',
			},
		},

		highlight: function (element) {
			$(element)
			.closest('.form-group')
			.addClass('has-error');
		},

		success: function (label) {
			label.closest('.form-group').removeClass('has-error');
			label.remove();
		},
	})
}

function checkPassword(username,password){
	var result = false;
	var body = {
		Username : username,
		Password : password,
	};
	$.ajax({
		url: 'http://localhost:3000/users/checkPassword',
		dataType: 'json',
		timeout: 10000,
		type: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(body),
	}).done(function(data) {
		alert('true');
		result = true;
	}).fail(function(xhr, textStatus, error) {
		console.log(textStatus);
		console.log(error);
		console.log(xhr);
	});
	return result;
}