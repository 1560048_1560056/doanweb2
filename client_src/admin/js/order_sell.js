$(document).ready(function(){
	$('#sa-success').hide();
	HandlebarsIntl.registerWith(Handlebars);

	$.ajax({
		url: 'http://localhost:3000/order/getAll',
		dataType: 'json',
		timeout: 10000
	}).done(function(data) {
		var source = '<tr><td id="fid">{{id}}</td><td>{{username}}</td><td align="center"><button type="button" class="btn btn-info btn-rounded w-xs waves-effect waves-light m-b-5 updateButton">Change</button></td></tr>';
		var template = Handlebars.compile(source);
		$('#datatable tbody > tr:first-child').remove();
		for(var i=0;i<data.length;i++){
			var x = {
				"id" : data[i].f_ID,
				"username" : data[i].f_Username,
			}
			var result = template(x);
			$("#list").append(result);
		}
	}).fail(function(xhr, textStatus, error) {
		console.log(textStatus);
		console.log(error);
		console.log(xhr);
	});
	return false;
});

$('#list').on('click', '.updateButton', function() {
	var tr = $(this).closest('tr');
	var id = $(this).closest('tr').find('#fid').text();
	var begin_date = new Date();
	begin_date = formatDateToDateTime(begin_date);
	var end_date = new Date();
	var day = end_date.getDate() + 7;
	end_date.setDate(day);
	end_date = formatDateToDateTime(end_date);
	var body = {
		BeginDate : begin_date,
		EndDate : end_date,
	};
	$.ajax({
		url: 'http://localhost:3000/users/updatePermission/' + id,
		dataType: 'json',
		timeout: 10000,
		type: 'put',
		contentType: 'application/json',
		data: JSON.stringify(body),
	}).done(function(data) {
		if(data.affectedRows == 1){
			$('#sa-success').trigger('click');
			removeChange(id);
			tr.remove();
		}
	}).fail(function(xhr, textStatus, error) {
		console.log(textStatus);
		console.log(error);
		console.log(xhr);
	});
	return false;
});

function removeChange(id){
	$.ajax({
		url: 'http://localhost:3000/order/' + id,
		dataType: 'json',
		timeout: 10000,
		type: 'delete',
	}).done(function(data) {
	}).fail(function(xhr, textStatus, error) {
		console.log(textStatus);
		console.log(error);
		console.log(xhr);
	});
}

function formatDateToDateTime(date){
	var s=date.getSeconds();
	var mn=date.getMinutes();
	var h=date.getHours();
	var d=date.getDate();
	var M=date.getMonth()+1;
	var Y=date.getFullYear();
	var date_time=`${Y}-${M}-${d} ${h}:${mn}:${s}`;
	return date_time
}