$('#submit').click(function() {
	checkValidate();
	if($('#frmAddCategory').valid())
	{
		var cat_name = $('#cat_name').val();
		var body = {
			CatName : cat_name,
		}
		$.ajax({
			url: 'http://localhost:3000/categories/',
			dataType: 'json',
			timeout: 10000,
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(body),
		}).done(function(data) {
			$('#sa-success').trigger('click');
			$('#frmAddCategory')[0].reset();
		}).fail(function(xhr, textStatus, error) {
			console.log(textStatus);
			console.log(error);
			console.log(xhr);
		});
	}
	return false;
})

function checkValidate(){
	$('#frmAddCategory').validate({
		rules: {
			cat_name: {
				required: true,
			}
		},
		messages: {
			cat_name: {
				required: 'Input category name',
			}
		},
		highlight: function (element) {
			$(element)
			.closest('.form-group')
			.addClass('has-error');
		},

		success: function (label) {
		},
	});
}