$(document).ready(function(){
	$('#sa-success').hide();
	HandlebarsIntl.registerWith(Handlebars);

	$.ajax({
		url: 'http://localhost:3000/categories/getAll',
		dataType: 'json',
		timeout: 10000
	}).done(function(data) {
		var source = '<tr><td id="cat_id">{{catid}}</td><td tabindex="1" id="cat_name">{{catname}}</td></td><td align="center"><button type="button" class="btn btn-info btn-rounded w-xs waves-effect waves-light m-b-5 delButton">Delete</button></td><td align="center"><button type="button" class="btn btn-danger btn-rounded w-xs waves-effect waves-light m-b-5 updateButton">Update</button></td></tr>';
		var template = Handlebars.compile(source);
		$('#mainTable tbody > tr:first-child').remove();
		for(var i=0;i<data.length;i++){
			var x = {
				"catid" : data[i].CatID,
				"catname" : data[i].CatName,
			}
			var result = template(x);
			$("#list").append(result);
		}
	}).fail(function(xhr, textStatus, error) {
		console.log(textStatus);
		console.log(error);
		console.log(xhr);
	});
	return false;
});

$('#list').on('click', '.delButton', function() {
	var tr = $(this).closest('tr');
	var id = $(this).closest('tr').find('#cat_id').text();
	$.ajax({
		url: 'http://localhost:3000/categories/' + id,
		dataType: 'json',
		timeout: 10000,
		type: 'delete',
	}).done(function(data) {
		tr.remove();
	}).fail(function(xhr, textStatus, error) {
		console.log(textStatus);
		console.log(error);
		console.log(xhr);
	});
});

$('#list').on('click', '.updateButton', function() {
	var id = $(this).closest('tr').find('#cat_id').text();
	var cat_name = $(this).closest('tr').find('#cat_name').text();
	var body = {
		CatName : cat_name,
	}
	$.ajax({
		url: 'http://localhost:3000/categories/update/' + id,
		dataType: 'json',
		timeout: 10000,
		type: 'put',
		contentType: 'application/json',
		data: JSON.stringify(body),
	}).done(function(data) {
		if(data.affectedRows == 1){
			$('#sa-success').trigger('click');
		}
	}).fail(function(xhr, textStatus, error) {
		console.log(textStatus);
		console.log(error);
		console.log(xhr);
	});
	return false;
});