$(document).ready(function(){
	$('#sa-basic').hide();
	HandlebarsIntl.registerWith(Handlebars);

	$.ajax({
		url: 'http://localhost:3000/users/getAll',
		dataType: 'json',
		timeout: 10000
	}).done(function(data) {
		var source = '<tr><td id="fid">{{id}}</td><td>{{username}}</td><td>{{fullname}}</td><td>{{email}}</td><td>{{permission}}</td><td>{{mark}}</td><td>{{address}}</td><td align="center"><button type="button" class="btn btn-info btn-rounded w-xs waves-effect waves-light m-b-5 delButton">Delete</button></td><td align="center"><button type="button" class="btn btn-danger btn-rounded w-xs waves-effect waves-light m-b-5 resetButton">Reset password</button></td></tr>';
		var template = Handlebars.compile(source);
		$('#datatable tbody > tr:first-child').remove();
		for(var i=0;i<data.length;i++){
			var x = {
				"id" : data[i].f_ID,
				"username" : data[i].f_Username,
				"fullname" : data[i].f_Name,
				"email" : data[i].f_Email,
				"permission" : data[i].f_Permission,
				"mark" : data[i].f_Mark,
				"address" : data[i].f_Address,
			}
			var result = template(x);
			$("#list").append(result);
		}
	}).fail(function(xhr, textStatus, error) {
		console.log(textStatus);
		console.log(error);
		console.log(xhr);
	});
	return false;
});

$('#list').on('click', '.delButton', function() {
	var tr = $(this).closest('tr');
	var id = $(this).closest('tr').find('#fid').text();
	$.ajax({
		url: 'http://localhost:3000/users/delete/' + id,
		dataType: 'json',
		timeout: 10000,
		type: 'delete',
	}).done(function(data) {
		tr.remove();
	}).fail(function(xhr, textStatus, error) {
		console.log(textStatus);
		console.log(error);
		console.log(xhr);
	});
});

$('#list').on('click', '.resetButton', function() {
	var id = $(this).closest('tr').find('#fid').text();
	var body = {
		Password : '123456',
	}
	$.ajax({
		url: 'http://localhost:3000/users/reset_password/' + id,
		dataType: 'json',
		timeout: 10000,
		type: 'put',
		contentType: 'application/json',
		data: JSON.stringify(body),
	}).done(function(data) {
		if(data.affectedRows == 1){
			$('#sa-basic').trigger('click');
		}
	}).fail(function(xhr, textStatus, error) {
		console.log(textStatus);
		console.log(error);
		console.log(xhr);
	});
	return false;
});
