var loadProductsbeing = function() {
    
    $.ajax({
        url: 'http://localhost:3000/being/'+localStorage.getItem('f_ID'),
        dataType: 'json',
        timeout: 10000
    }).done(function(data) {
         var sr=[];
        for(var i=0;i<data.products.length;i++){
            sr.push(data.products[i].ProName);
        }
        $( "#txtsearch2" ).autocomplete({
             source: sr
        });
        var source = $('#product-template').html();
        var template = Handlebars.compile(source);
        //var bestprice=[data.products[0],data.products[1],data.products[2],data.products[3],data.products[4]];
        var html = template(data.products);

        $('#Being-product-list').append(html);
        EditText('.h4name','h4',30,'""');
        EditText('.ptinydes','p',60,'"height:36px;"');

        $('#Being-product-list div[style]').fadeIn(2000, function() {
            $(this).removeAttr('style');
        });
        $('.new').hide();
        shownew();
        if(data!=null&&data.products.length>0){
            autochangedatetime(data.products);
        }
        $('.viewproduct').off("click");
        $('.viewproduct').on("click",function(){
            loadProductsDetail($(this));
            $('#myModal').modal('show');
        }); 

        
    });
};

$(function(){
    

    loadProductsbeing()

    $('#btnsearch2').on('click',function(){
        $.ajax({
            type:'POST',
            url: 'http://localhost:3000/being/search',
            dataType: 'json',
            timeout: 10000,           
            contentType: 'application/json',
            data: JSON.stringify({name: $('#txtsearch2').val(),idu:localStorage.getItem('f_ID')}),
        }).done(function(data) {
             $('.product').remove();
            var source = $('#product-template').html();
            var template = Handlebars.compile(source);
            //var bestprice=[data.products[0],data.products[1],data.products[2],data.products[3],data.products[4]];
            var html = template(data.products);

            $('#Being-product-list').append(html);
            EditText('.h4name','h4',30,'""');
            EditText('.ptinydes','p',60,'"height:36px;"');

            $('#Being-product-list div[style]').fadeIn(2000, function() {
                $(this).removeAttr('style');
            });
            $('.new').hide();
            shownew();
            if(data!=null&&data.products.length>0){
                autochangedatetime(data.products);
            }
            $('.viewproduct').off("click");
            $('.viewproduct').on("click",function(){
                loadProductsDetail($(this));
                $('#myModal').modal('show');
            }); 

            
        });
    });
});